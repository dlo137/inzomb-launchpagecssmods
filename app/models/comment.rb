class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :images, :foreign_key => :image_id
  has_many :votes

  validates_presence_of :user_id, :image_id, :comment_text

  def vote_exists(user)
    upvote_exists(user) || downvote_exists
  end
  def upvote_exists(user)
    return false if user.nil?
    self.votes.exists?(:user_id => user.id, :upvote => true)
  end

  def downvote_exists(user)
    return false if user.nil?
    self.votes.exists?(:user_id => user.id, :upvote => false)
  end
  def upvote
  end
  def downvote
  end
  def score
  end
end
