class Vote < ActiveRecord::Base
  belongs_to :user, :foreign_key => :user_id
  belongs_to :images, :foreign_key => :image_id
  belongs_to :comment, :foreign_key => :comment_id

  def self.image_upvote_count(image_id)
    self.where(:image_id => image_id, :upvote => true).count
  end
  def self.image_downvote_conut(image_id)
    self.where(:image_id => image_id, :upvote => false).count
  end

  def self.comment_upvote_count(comment_id)
    self.where(:comment_id => comment_id, :upvote => true).count
  end

  def self.comment_downvote_count(comment_id)
    self.where(:comment_id => comment_id, :upvote => false).count
  end
  def self.comment_score(comment_id)
    self.comment_upvote_count(comment_id) - Vote.comment_downvote_count(comment_id)
  end
end
