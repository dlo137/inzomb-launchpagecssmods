module ApplicationHelper
  def profile_link(user)
    link_to( user.name, "/profiles/#{user.name}" )
  end
end
