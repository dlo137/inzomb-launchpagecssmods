class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def index
    @users = User.all
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      session[:user_id] = @user.id
      redirect_to  "/image/new", :notice => "Thanks for signing up!"
    else
      render "new"
    end
  end
  def profiles
    @user = User.find_by_name(params[:username])
    if @user
      @images = Vote.where(:user_id => @user.id).limit(10).order("created_at desc").collect {|x| x.images }
    else
      redirect_to :controller => :image, :action => :index, :notice => "Invalid User Profile"
    end
  end
end
