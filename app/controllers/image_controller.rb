class ImageController < ApplicationController
  before_filter :authenticate, :except => [:index, :votes, :recent]

  def authenticate
    if !current_user
      redirect_to :controller => :sessions, :action => :new, :notice => "Please login before uploading image"
    end
  end

  def index
    recent
  end
  def recent
    if current_user.nil?
      @logged = false
    end
    @category = :recent
    @images = Images.ten_most_recent
    @comment = Comment.new
  end
  def new_comment
    @comment = Comment.new(params[:comment])
    if @comment.save
      redirect_to :action => :index, :notice => "Comment successful"
    else
    end
  end
  def votes
    @category = :votes
    @images = Images.ten_most_votes
    render :index
  end
  def upvote
    image = Images.find_by_id(params[:id])
    image.upvote
    Vote.create!( :user => current_user, :images => image, :upvote => true)
    redirect_to images_path
  end
  def comment_upvote
    comment = Comment.find_by_id(params[:id])
    comment.upvote
    Vote.create!( :user => current_user, :comment => comment, :upvote => true)
    redirect_to images_path
  end
  def comment_downvote
    comment = Comment.find_by_id(params[:id])
    comment.downvote
    Vote.create!( :user => current_user, :comment => comment, :upvote => false)
    redirect_to images_path
  end
  def flagPicture
    image = Images.find_by_id(params[:id])
    UserMailer.flagPicture(image, current_user).deliver
    redirect_to images_path
  end
  def downvote
    image = Images.find_by_id(params[:id])
    image.downvote
    Vote.create!( :user => current_user, :images => image, :upvote => false)
    redirect_to images_path
  end
  def create
    @image = Images.new(params[:image])
    @image.user_id = current_user.id
    @image.upscore = 0
    @image.downscore = 0
    if @image.save
      redirect_to :action => :index, :notice => "Image Successfully Uploaded"
    else
      render "new"
    end

  end
  def new
    @image = Images.new
  end

end
