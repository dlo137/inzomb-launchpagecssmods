class BetaController < ApplicationController
  def index
    render :layout => false
  end

  def validate
    firstName = params[:first_name]
    lastName = params[:last_name]

    user = BetaUser.validateBetaUser(firstName, lastName)
    if(user)
      user.HasLoggedIn = true
      user.save
      redirect_to :controller => 'image', :action => 'index'
    else
      redirect_to('/access_denied.html')
    end

  end

  def new
    @beta = BetaUser.new
  end
end
