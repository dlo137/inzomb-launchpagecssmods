class AddFingerprintColumnToImages < ActiveRecord::Migration
    def self.up
      add_column :images, :fingerprint, :string
    end

    def self.down
      remove_column :images, :fingerprint
    end
end
